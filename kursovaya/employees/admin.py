from django.contrib import admin
from .models import *
from import_export.admin import ExportActionMixin


class EmployeesAdmin(ExportActionMixin, admin.ModelAdmin):
    list_display = ('id', 'surname', 'name', 'patronymic', 'jobTitle')
    list_display_links = ('id', 'surname')
    search_fields = ('surname', 'department')
    list_filter = ('department' , 'jobTitle', 'salary')
    class Meta:
        model = Employees

class JobTitleAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')
    list_display_links = ('id', 'name')

class DepartmentAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')
    list_display_links = ('id', 'name')

class SalaryAdmin(admin.ModelAdmin):
    list_display = ('id', 'sum')
    list_display_links = ('id', 'sum')

class EducationAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')
    list_display_links = ('id', 'name')

class PremiumAdmin(admin.ModelAdmin):
    list_display = ('id', 'sum')
    list_display_links = ('id', 'sum')



admin.site.register(Employees, EmployeesAdmin)
admin.site.register(JobTitle, JobTitleAdmin)
admin.site.register(Department, DepartmentAdmin)
admin.site.register(Salary, SalaryAdmin)
admin.site.register(Education, EducationAdmin)
admin.site.register(Premium, PremiumAdmin)