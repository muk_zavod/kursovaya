from django.urls import path, include
from .views import *


from rest_framework import routers

# urlpatterns = [
#     path('', index, name='index'),
#     # path('api/v1/employees/', employees, name='employees'),
#     path('api/v1/employees/', EmployeeAPIList.as_view(), name='employees'),
#     # path('api/v1/employee/<int:employee_id>/', show_employee, name='employee'),
#     path('api/v1/employees/<int:employee_id>/', EmployeeAPIList.as_view(), name='employee'),
#     path('api/v1/employeesdetail/<int:employee_id>/', EmployeeAPIDetailView.as_view()),
#     path('login/', LoginUser.as_view(), name='login'),
#     path('logout/', logout_user, name='logout'),
#     path('register/', RegisterUser.as_view(), name='register'),
#     path('api/v1/addemployee/', addEmployee, name='addemployee')
# ]



router = routers.DefaultRouter()
router.register(r'employees', EmployeesViewSet, basename='employees')


urlpatterns = [
    path('api/v1/', include(router.urls)),  # http://127.0.0.1:8000/api/v1/employees/
    path('', index, name='index'),
    path('login/', LoginUser.as_view(), name='login'),
    path('logout/', logout_user, name='logout'),
    path('register/', RegisterUser.as_view(), name='register'),
    path('addemployee/', addEmployee, name='addemployee')
]
