from django.db import models
from django.urls import reverse
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator


class Employees(models.Model):
    surname = models.CharField(max_length=25, verbose_name="Фамилия")
    name = models.CharField(max_length=255, verbose_name="Имя")
    patronymic = models.CharField(max_length=255, verbose_name="Отчество")
    age = models.PositiveSmallIntegerField(verbose_name="Возраст")
    dateOfBirth = models.DateField(max_length=255, verbose_name="Дата рождения", blank=True)
    jobTitle = models.ForeignKey('JobTitle', on_delete=models.PROTECT, verbose_name="Должность")
    department = models.ForeignKey('Department', on_delete=models.PROTECT, verbose_name="Отдел")
    salary = models.ForeignKey('Salary', on_delete=models.PROTECT, verbose_name="Зарплата")
    telephone = models.CharField(max_length=255, verbose_name="Телефон")
    address = models.CharField(max_length=255, verbose_name="Адрес", validators=[
        RegexValidator(
            regex='^[a-z]*$',
            message='Адрес должен содержат цифры',
            code='invalid_username'
        ),
    ])
    education = models.ForeignKey('Education', on_delete=models.PROTECT, verbose_name="Образование")
    gender = models.CharField(max_length=255, verbose_name="Пол")
    photo = models.ImageField(upload_to="photos/%Y/%m/%d/", verbose_name="Фотография")
    premium = models.ForeignKey('Premium', on_delete=models.PROTECT, verbose_name="Премия")

    def __str__(self):
        return self.surname

    def get_absolute_url(self):
        return reverse('employee', kwargs={'employee_id': self.pk})

    class Meta:
        verbose_name = "Сотрудники"
        verbose_name_plural = "Сотрудники"
        ordering = ['surname']

class JobTitle(models.Model):
    name = models.CharField(max_length=200, db_index=True, verbose_name="Название должности")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Должность"
        verbose_name_plural = "Должность"
        ordering = ['id']

class Department(models.Model):
    name = models.CharField(max_length=200, db_index=True, verbose_name="Название отдела")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Отдел"
        verbose_name_plural = "Отдел"
        ordering = ['id']

class Salary(models.Model):
    sum = models.CharField(max_length=200, db_index=True, verbose_name="Размер зарплаты")

    def __str__(self):
        return self.sum

    class Meta:
        verbose_name = "Зарплата"
        verbose_name_plural = "Зарплата"
        ordering = ['id']

class Education(models.Model):
    name = models.CharField(max_length=200, db_index=True, verbose_name="Образование")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Образование"
        verbose_name_plural = "Образование"
        ordering = ['id']

class Premium(models.Model):
    sum = models.CharField(max_length=200, db_index=True, verbose_name="Размер премии")

    def __str__(self):
        return self.sum

    class Meta:
        verbose_name = "Премия"
        verbose_name_plural = "Премия"
        ordering = ['id']
