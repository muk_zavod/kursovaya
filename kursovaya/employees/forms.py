from django import forms
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError

from .models import *


from tkinter import Widget

from django.db import models

from django.forms import DateInput, NumberInput, TextInput, CharField, ChoiceField, ModelForm, DateField, RadioSelect, ImageField



Choice_gender = [('1', 'женщина'), ('2', 'мужчина')]
Choice_department = [('1', 'маркетинг'), ('2', 'дизайн'), ('3', 'разработка'), ('4', 'управление')]
Choice_education = [('1', 'высшее образование'), ('2', 'среднее образование'),
                    ('3', 'среднее профессиональное образование')]
Choice_jobTitle = [('1', 'дизайнер'), ('2', 'главный дизайнер'), ('3', 'разработчик'), ('4', 'главный разработчик'),
                   ('5', 'проект-менеджер'), ('6', 'директор'), ('7', 'маркетолог'), ('8', 'главный маркетолог'),
                   ('9', 'hr-специалист'), ('10', 'менеджер по работе с клиентами')]
Choice_premium = [('1', '1000'), ('2', '3000'), ('3', '5000'), ('4', '0')]
Choice_salary = [('1', '70000'), ('2', '250000'), ('3', '120000'), ('4', '170000'), ('4', '300000')]


class EmployeesForm(ModelForm):


    surname = CharField(label='Фамилия', widget=TextInput())
    name = CharField(label='Имя', widget=TextInput())
    patronymic = CharField(label='Отчество', widget=TextInput())
    age = CharField(label='Возраст', widget=NumberInput())

    dateOfBirth = DateField(label='Дата рождения', widget=DateInput())

    address = CharField(label='Адрес', widget=TextInput())
    gender = ChoiceField(label='Гендер', widget=RadioSelect, choices=Choice_gender)
    photo = ImageField()
    department = models.ForeignKey('Department', on_delete=models.PROTECT)
    jobTitle = models.ForeignKey('JobTitle',on_delete=models.PROTECT)
    salary = models.ForeignKey('Salary',  on_delete=models.PROTECT)
    education = models.ForeignKey('Education' , on_delete=models.PROTECT)
    premium = models.ForeignKey('Premium', on_delete=models.PROTECT)
    telephone = CharField(label='Телефон', widget=NumberInput(attrs={'type': 'tel'}))


    class Meta:
        model = Employees
        fields = ['surname', 'name', 'patronymic', 'age', 'dateOfBirth', 'address', 'gender', 'photo', 'department', 'education', 'jobTitle', 'premium', 'salary', 'telephone']



class JobTitle(ModelForm):
    name = ChoiceField(label='Должность', choices=Choice_jobTitle)
    class Meta:
        model = JobTitle
        fields = ['name']
class Department(ModelForm):
    name = ChoiceField(label='Отдел', choices=Choice_department)
    class Meta:
        model = Department
        fields = ['name']

class Salary(ModelForm):
    sum = ChoiceField(label='Зарплата', choices=Choice_salary)

    class Meta:
        model = Salary
        fields = ['sum']

class Education(ModelForm):
    name = ChoiceField(label='Образование', choices=Choice_education)
    class Meta:
        model = Education
        fields = ['name']

class Premium(ModelForm):
    sum = ChoiceField(label='Премия', choices=Choice_premium)

    class Meta:
        model = Premium
        fields = ['sum']








class RegisterUserForm(UserCreationForm):
    username = forms.CharField(label='Логин', widget=forms.TextInput(attrs={'class': 'form-input'}))
    email = forms.EmailField(label='Email', widget=forms.EmailInput(attrs={'class': 'form-input'}))
    password1 = forms.CharField(label='Пароль', widget=forms.PasswordInput(attrs={'class': 'form-input'}))
    password2 = forms.CharField(label='Повтор пароля', widget=forms.PasswordInput(attrs={'class': 'form-input'}))

    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2')

class LoginUserForm(AuthenticationForm):
    username = forms.CharField(label="Логин", widget=forms.TextInput(attrs={'class': 'form-input'}))
    password = forms.CharField(label="Пароль", widget=forms.PasswordInput(attrs={'class': 'form-input'}))
