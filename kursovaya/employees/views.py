from django.contrib.auth import logout, login
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.views import LoginView
from django.core.paginator import Paginator
from django.http import HttpResponse, HttpResponseNotFound
from django.shortcuts import render, redirect
from .models import *
from django.urls import reverse_lazy
from django.views.generic import CreateView
from .forms import *
from .models import *
from .utils import *
from django.forms import model_to_dict
from rest_framework import generics, viewsets
from django.shortcuts import render
from rest_framework.response import Response
from .serializers import EmployeeSerializer
from rest_framework.pagination import PageNumberPagination
from rest_framework.decorators import action
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters
from django.db.models import Q


menu = [{'title':"Главная", 'url_name':'index'},
        {'title':"Сотрудники", 'url_name':'employees'},
        {'title':"Войти", 'url_name':'login'},
        {'title':"Регистрация", 'url_name':'register'},
        {'title':"Сотрудник", 'url_name':'employee'},
]

def index(request):
    return render(request, 'employees/index.html', {'menu' : menu})


class EmployeesPagination(PageNumberPagination):
    page_size = 8

class EmployeesViewSet(viewsets.ModelViewSet):
    serializer_class = EmployeeSerializer
    pagination_class = EmployeesPagination

    def get_queryset(self):
        pk = self.kwargs.get("pk")
        if not pk:
            return Employees.objects.all()
        return Employees.objects.filter(pk=pk)




    filter_backends = [DjangoFilterBackend, filters.SearchFilter]
    filterset_fields = ['age']
    
    search_fields = ['surname', 'name']



    @action(methods=['get'], detail=False)
    def departments(self, request):
        departments = Department.objects.all()
        return Response({'departments': [c.name for c in departments]})


    @action(methods=['get'], detail=False)
    def q(self, request):
        departments = Employees.objects.filter(
            Q(name__startswith='В') | Q(name__startswith='И')
        )
        return Response({'departments': [c.name for c in departments]})


    # @action(detail=True, methods=['post'])
    # def set_password(self, request, pk=None):
    #     user = self.get_object()
    #     serializer = PasswordSerializer(data=request.data)
    #     if serializer.is_valid():
    #         user.set_password(serializer.validated_data['password'])
    #         user.save()
    #         return Response({'status': 'password set'})
    #     else:
    #         return Response(serializer.errors,
    #                         status=status.HTTP_400_BAD_REQUEST)




class RegisterUser(DataMixin, CreateView):
    form_class = RegisterUserForm
    template_name = 'employees/register.html'
    success_url = reverse_lazy('login')

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        c_def = self.get_user_context(title="Регистрация")
        return dict(list(context.items()) + list(c_def.items()))

    def form_valid(self, form):
        user = form.save()
        login(self.request, user)
        return redirect('index')

class LoginUser(DataMixin, LoginView):
    form_class = LoginUserForm
    template_name = 'employees/login.html'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        c_def = self.get_user_context(title="Авторизация")
        return dict(list(context.items()) + list(c_def.items()))

    def get_success_url(self):
        return reverse_lazy('index')


def logout_user(request):
    logout(request)
    return redirect('login')



def pageNotFound(request, exception):
    return HttpResponseNotFound('<h1>Страница не найдена</h1>')


def addEmployee(request):
    error = ""

    if request.method == "POST":

        form = EmployeesForm(request.POST)
        form.save()
        return redirect('addemployee')

    else:
        error = "форма заполнена неверно!"

    form = EmployeesForm()

    data = {
        "form": form, 
        "error": error
    }

    return render(request, 'employees/addemployee.html' , data)