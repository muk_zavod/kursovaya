from rest_framework import serializers

from .models import *
from drf_writable_nested import WritableNestedModelSerializer 



class DepartmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Department
        fields = ['name']
class EducationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Education
        fields = ['name']
class JobTitleSerializer(serializers.ModelSerializer):
    class Meta:
        model = JobTitle
        fields = ['name']
class PremiumSerializer(serializers.ModelSerializer):
    class Meta:
        model = Premium
        fields = ['sum']
class SalarySerializer(serializers.ModelSerializer):
    class Meta:
        model = Salary
        fields = ['sum']


class EmployeeSerializer(WritableNestedModelSerializer , serializers.ModelSerializer):

    department = DepartmentSerializer(read_only=False)
    education = EducationSerializer(read_only=False)
    jobTitle = JobTitleSerializer(read_only=False)
    premium = PremiumSerializer(read_only=False)
    salary = SalarySerializer(read_only=False)

    class Meta:
        model = Employees
        fields = ["id", "surname", "name", "patronymic", "age", "dateOfBirth", "telephone", "address","gender","photo","jobTitle","department","salary","education","premium"]
        



